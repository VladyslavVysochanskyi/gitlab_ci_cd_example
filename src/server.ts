import express, { Request, Response } from 'express';
import serverless from 'serverless-http';

import { getTimestamp } from './utils';

const app = express();
const router = express.Router();

router.get('/', (req: Request, res: Response) => {
  res.send(`Express + TypeScript Server. Timestamp: ${getTimestamp()}`);
});

app.use('/.netlify/functions/server', router);
export const handler = serverless(app);