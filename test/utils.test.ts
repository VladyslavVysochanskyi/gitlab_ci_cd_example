import { getTimestamp } from '../src/utils';

describe('getTimestamp', () => {
    it('result type should be number', () => {
        const result = getTimestamp();

        expect(typeof result).toEqual('number');
    });
});